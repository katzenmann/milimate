use rocket_db_pools::{Database, Connection};
use rocket_db_pools::diesel::{QueryResult, PgPool, prelude::*};

#[derive(Database)]
#[database("public")]
pub stuct PublicDb(PgPool);

#[derive(Queryable, Insertable)]
#[diesel(table_name = avatar)]
pub struct Avatar {
    id: i64,
    ident: char,
    data: Vec<u8>,
    hash: Vec<u8>,
}

diesel::table! {
    avatar (id) {
        id -> BigInt,
        ident -> Char,
        data -> Array<u8>,
        hash -> Array<u8>,
    }
}

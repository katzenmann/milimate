#[macro_use] extern crate rocket;
use db::{PublicDb, Avatar};

mod db;

#[get("/")]
async fn list(mut db: Connection<PublicDb>) -> QueryResult<String> {
    avatar::table.load(&mut db)
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index])
}
